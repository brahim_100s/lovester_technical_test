#!/bin/bash

# Set up a MySQL Docker container
docker run --name mysql -e MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD -d mysql

# Create a new database
docker exec -it mysql mysql -u root -p$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE $db_name"

# Create a new MySQL user
docker exec -it mysql mysql -u root -p$MYSQL_ROOT_PASSWORD -e "CREATE USER '$user'@'%' IDENTIFIED BY '$user_pwd'; GRANT ALL PRIVILEGES ON $db_name.* TO '$user'@'%'; FLUSH PRIVILEGES;"