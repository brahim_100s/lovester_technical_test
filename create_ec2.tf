provider "aws" {
  region = "$region"
}

resource "aws_instance" "crm_instance" {
  ami           = "$crm_ami"
  instance_type = "t2.micro"
  key_name      = "$crm_key"
  security_groups = [$crm_sg_id]
}

resource "aws_security_group" "$crm_sg" {
  name_prefix = "crm-sg"
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}