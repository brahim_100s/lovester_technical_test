#!/bin/bash

# Step 1: Create and run the SuiteCRM container
docker run --name suitecrm -p 80:80 -e DB_TYPE=mysql -e DB_HOST=mysql -e DB_PORT=3306 -e DB_NAME=$db_name -e DB_USER=$user -e DB_PASSWORD=$user_pwd -d suitecrm/suitecrm

# Step 2: Wait for the SuiteCRM container to start up
echo "Waiting for SuiteCRM container to start up..."
sleep 30
echo "SuiteCRM is up and running"

# Step 3: Install any necessary dependencies or modules
docker exec suitecrm bash -c "php composer.phar install --no-dev"

# Step 4: Configure the application settings
docker exec suitecrm bash -c "cp config_override.php.dist config_override.php"
docker exec suitecrm bash -c "sed -i 's/\$sugar_config\['db_host_name'\] = ''/\$sugar_config\['db_host_name'\] = '$mysql'/g' config_override.php"
docker exec suitecrm bash -c "sed -i 's/\$sugar_config\['db_user_name'\] = ''/\$sugar_config\['db_user_name'\] = '$user'/g' config_override.php"
docker exec suitecrm bash -c "sed -i 's/\$sugar_config\['db_password'\] = ''/\$sugar_config\['db_password'\] = '$user_pwd'/g' config_override.php"
docker exec suitecrm bash -c "sed -i 's/\$sugar_config\['db_name'\] = ''/\$sugar_config\['db_name'\] = '$db_name'/g' config_override.php"

echo "SuiteCRM has been successfully configured"