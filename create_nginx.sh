#!/bin/bash

# Generate the SSL certificate
openssl req -newkey rsa:2048 -nodes -keyout server.key -x509 -days 365 -out server.crt -subj "/C=US/ST=California/L=San Francisco/O=Example Inc./CN=example.com"

# Create the nginx.conf file with SSL configuration
cat > nginx.conf <<EOF
server {
        listen 443 ssl;
        server_name $server_name;
        ssl_certificate /etc/nginx/certs/server.crt;
        ssl_certificate_key /etc/nginx/certs/server.key;
EOF

# Start an Nginx container with SSL configuration
docker run -d \
  --name nginx \
  -p 80:80 \
  -p 443:443 \
  --link suitecrm:crm \
  -v server.crt:/etc/nginx/certs/server.crt \
  -v server.key:/etc/nginx/certs/server.key \
  -v nginx.conf:/etc/nginx/nginx.conf \
  nginx